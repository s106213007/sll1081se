<?php
require_once("dbconfig.php");
function ListProduct() {
	global $db;
	$sql = "select * from product;";
	$stmt = mysqli_prepare($db, $sql );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	return $result;
}

//$status = 0為購物車中的商品，1為以結帳的訂單紀錄
function AddToCart($pID, $quantity, $status=0){
	global $db;
	session_start();
	$ID = $_SESSION["loginProfile"]["ID"];
	//$sql1 = "SELECT COUNT(*) FROM cart where ID = '$ID' and pid = '$pID';";
	$sql1 ="SELECT * FROM cart WHERE ID='$ID'and pid = '$pID' and status = 0;";
	$sql2 = "INSERT INTO `cart` (ID,pID,quantity,status) VALUES ('$ID','$pID','$quantity','$status');";
	$sql3 = "UPDATE cart SET quantity = quantity + '$quantity' WHERE pID = '$pID' and ID = '$ID';";
	$result = mysqli_query($db,$sql1);
	//if(mysqli_fetch_assoc($result) == 0)
	if(mysqli_num_rows($result) == 0)
	{
		mysqli_query($db,$sql2);
	}
	else
	{
		mysqli_query($db,$sql3);
	}
}

function ChangeQuantity($pID, $quantity, $status=0){
	global $db;
	session_start();
	$ID = $_SESSION["loginProfile"]["ID"];
	//$sql1 = "SELECT COUNT(*) FROM cart where ID = '$ID' and pid = '$pID';";
	$sql1 ="SELECT * FROM cart WHERE ID='$ID'and pid = '$pID';";
	$sql3 = "UPDATE cart SET quantity = '$quantity' WHERE pID = '$pID' and ID = '$ID';";
	$result = mysqli_query($db,$sql1);
	mysqli_query($db,$sql3);

}

function DeleteFromCart($pID){
	global $db;
	session_start();
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql = "DELETE FROM cart WHERE pID = '$pID' and ID = '$ID';";
	mysqli_query($db,$sql);
}

function ShowCart()
{
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql = "SELECT product.pID,product.pName,product.pPrice,cart.quantity,product.pPrice*cart.quantity FROM `cart`,`product` where cart.ID = '$ID' and cart.pID = product.pID and status = 0;";
	$stmt = mysqli_prepare($db, $sql );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	return $result;
}
//mysqli_insert_id(抓最新insert進去的那筆資料的流水號)
//所以function getordID($ID) 其實等於 $M = mysqli_insert_id($db);
function getordID($ID){
	global $db;
	//取orderlist中的ID = $ID 的ordID最大值設為變數M(update cart的OrdID用)
	$sql1 = "select Max(ordID) from orderlist where ID = '$ID';";
	$stmt = mysqli_prepare($db, $sql1 );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	while (	$rs=mysqli_fetch_assoc($result)){
		$M = $rs['Max(ordID)'];
	}
	return $M;
}

function CheckOut_3(){
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql ="SELECT * FROM cart WHERE ID='$ID' and status = 0;";
	$result = mysqli_query($db,$sql);
	if(mysqli_num_rows($result) != 0){
		return true;
	}
}

function CheckOut_1($address, $Tstatus = 0) {
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql2 = "INSERT INTO orderlist (ID,address,Tstatus) VALUES ('$ID','$address','$Tstatus');";
	$stmt = mysqli_prepare($db, $sql2 );
	return mysqli_stmt_execute($stmt);
}


function CheckOut_2($address, $Tstatus = 0) {
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$MaxOrdID = getordID($ID);
	$sql3 = "update cart set status = 1,ordID = '$MaxOrdID' where ID = '$ID' and status = 0;";
	$stmt = mysqli_prepare($db, $sql3 );
	return mysqli_stmt_execute($stmt);
}

function ShowOrders(){
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql1 = "SELECT ordID,Date,address,Tstatus FROM orderlist WHERE ID='$ID';";
	$stmt = mysqli_prepare($db, $sql1 );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	return $result;
	}

function OrderDetail($ordID){
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql1 = "SELECT product.pID,product.pName,product.pPrice,cart.quantity FROM product,cart WHERE cart.ID='$ID' and cart.ordID = $ordID and cart.pID = product.pID;";
	$stmt = mysqli_prepare($db, $sql1 );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	return $result;
}

function CheckOrderStatus($ordID){
	global $db;
//問號 //?表示是變數(傳過來的$ID;$passWord)
	$sql1 = "select * from orderlist where ordID = $ordID;";
	$stmt = mysqli_prepare($db, $sql1); //prepare sql statement
	//"ss" s表示字串 第一個s是ID是字串;第二個s是password 是字串
	//把sql指令和兩個string變數("ss")$ID、$password綁在一起
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		//return user profile
		$ret=array('ordID' => $ordID, 'ID' => $row['ID'], 'Date' => $row['Date'], 'address' => $row['address'], 'Tstatus' => $row['Tstatus']);
	} else {
		//ID, password are not correct
		$ret=NULL;
	}
	return $ret;
}

function DeleteMyOrder($ordID){
	global $db;
	session_start();
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql1 = "DELETE FROM orderlist WHERE ordID = '$ordID' and ID = '$ID';";
	$sql2 = "UPDATE user SET Bad = Bad + 1 WHERE ID = '$ID';";
	$sql3 = "UPDATE cart SET status = 0 WHERE ID = '$ID' and ordID = '$ordID';";
	if(mysqli_query($db,$sql1)){
		mysqli_query($db,$sql2);
		mysqli_query($db,$sql3);
	}else{
		echo "Sorry! Delete Fail.";
	}
}

function FindOrder() {
	global $db;
	$ID = $_SESSION["loginProfile"]["ID"];
	$sql = "select * from user where ID = '$ID';";
	$stmt = mysqli_prepare($db, $sql );
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	while (	$rs=mysqli_fetch_assoc($result)){
		$Z = $rs['Bad'];
	}
	return $Z;
}

?>










