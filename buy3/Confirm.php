<?php
session_start();
require("ShopModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Examples</title>
</head>
<body>
<p>Your Order detail is : 
[<a href="loginUI.php">登出</a>]
</p>
<hr>
<?php
	if($_SESSION['loginProfile']['role'] == 0)
	{
		$_SESSION['ShowRole'] = "customer";
	}else{
		$_SESSION['ShowRole'] = "admin";
	}
	echo "Hello ", $_SESSION["ShowRole"],"&nbsp", $_SESSION["loginProfile"]["Name"],
	", Your ID is: ", $_SESSION["loginProfile"]["ID"],
	"<HR>";
	$result=ShowCart();
?>

	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>產品名稱</td>
    <td>價錢</td>
    <td>數量</td>
    <td>小計</td>
  </tr>
<?php
$total=0;
$ID = $_SESSION['loginProfile']['ID'];
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<td>",$ID,"</td>";
	echo "<td>{$rs['pName']}</td>";
	echo "<td>" , $rs['pPrice'], "</td>";
	echo "<td>" , $rs['quantity'], "</td>";
	$total += $rs['quantity'] *$rs['pPrice'];
	echo "<td>" , $rs['quantity'] *$rs['pPrice'] , "</td>";
	echo "</tr>";
}
echo "<tr><td>Total: $total</td></tr>";
?>
</table>
<hr>
<form action="CheckOut.php" method="post">
請輸入寄送地址: <input type="text" name="address" required><br>
<input type="submit" value="確認訂單">
<br/>
<br/>
<a href="main.php">繼續購物</a>
</form>
</body>
</html>